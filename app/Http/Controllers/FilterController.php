<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilterController extends Controller
{

    public function index(Request $request)
    {
        $jsonString = file_get_contents(base_path('\public\storage\reviews(3).json'));

        $reviews = json_decode($jsonString, true);

        $reviews = collect($reviews);

        if ($request->has('minRating')) {
            $minRating = intval($request->minRating);
            $reviews = $reviews->whereBetween('rating', [$minRating, 5]);
        }

        if ($request->has('dateFromOldest')) {
            if ($request->dateFromOldest) {
                $reviews = $reviews->sortBy('reviewCreatedOnTime');
            } else {
                $reviews = $reviews->sortBy('reviewCreatedOnTime',SORT_REGULAR,true);
            }
        }

        if ($request->has('ratingLowestFirst')) {
            if ($request->ratingLowestFirst) {
                $reviews = $reviews->sortBy('rating');
            } else {
                $reviews = $reviews->sortBy('rating',SORT_REGULAR,true);
            }
        }

        if ($request->has('textPriority')) {
            if ($request->textPriority) {
                $reviews = $reviews->sortBy('reviewText',SORT_REGULAR,true);
            }
        }

        return view('index', compact('reviews'));
    }
}
