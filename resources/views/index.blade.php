@extends('layouts.main')

@section('content')
<div class="my-10 mx-auto px-5">
    <h2 class="text-xl">Filter reviews</h2>
    <form method="GET" class="my-10 mx-auto w-3/4">
        @csrf
        <div class="mb-6">
            <label for="ratingLowestFirst" class="block mb-2 text-sm font-medium text-gray-900">Order by rating:</label>
            <select id="ratingLowestFirst" name="ratingLowestFirst"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                <option disabled selected>Select from options</option>
                <option value="0">Highest First</option>
                <option value="1">Lowest First</option>
            </select>
        </div>
        <div class="mb-6">
            <label for="minRating" class="block mb-2 text-sm font-medium text-gray-900">Maximum rating:</label>
            <select id="minRating" name="minRating"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                <option disabled selected>Select from options</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>
        <div class="mb-6">
            <label for="dateFromOldest" class="block mb-2 text-sm font-medium text-gray-900">Order by date:</label>
            <select id="dateFromOldest" name="dateFromOldest"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                <option disabled selected>Select from options</option>
                <option value="0">Newest First</option>
                <option value="1">Oldest First</option>
            </select>
        </div>
        <div class="mb-6">
            <label for="textPriority" class="block mb-2 text-sm font-medium text-gray-900">Prioritize by text:</label>
            <select id="textPriority" name="textPriority"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                <option disabled selected>Select from options</option>
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
        </div>
        <button type="submit"
            class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center">Filter</button>
    </form>

    <div class="cards">
        @foreach ($reviews as $review)
        <div class="block p-6 mb-5 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900">{{$review['reviewText']}}</h5>
            <p class="font-normal text-gray-700">{{$review['reviewFullText']}}</p>
            <div class="flex items-center justify-between mt-3">
                <div class="flex items-center">
                    <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <title>Rating star</title>
                        <path
                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z">
                        </path>
                    </svg>
                    <p class="ml-2 text-sm font-bold text-gray-900 dark:text-white">{{$review['rating']}}</p>
                </div>
                <p class="text-xs text-right">{{$review['reviewCreatedOnDate']}}</p>
            </div>
        </div>
        @endforeach
    </div>

</div>
@endsection